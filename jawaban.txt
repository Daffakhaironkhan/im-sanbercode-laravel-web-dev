1. Membuat Database

CREATE DATABASE myshop;


2.Membuat Table di Dalam Database

- Tabel User
CREATE TABLE users(
    id int(11) AUTO_INCREMENT PRIMARY KEY,
    name varchar(255) NOT null,
    email varchar(255) NOT null,
    password varchar(255) NOT null,
);

- Tabel Categories
CREATE TABLE categories(
    id int(11) AUTO_INCREMENT PRIMARY KEY,
    name varchar(255) NOT null
);

- Tabel items
CREATE TABLE items(
    id int(11) AUTO_INCREMENT PRIMARY KEY,
    name varchar(255) NOT null,
    description varchar(255) NOT null,
    price int(11) NOT null,
    stock int(11) NOT null,
    category_id int(11) NOT null,
    FOREIGN KEY(category_id) REFERENCES users(id)
);



3. Memasukkan Data pada Table

- Tabel User
INSERT INTO users (name, email, password) VALUES ("John Doe", "john@doe.com", "john123"), ("John Doe", "john@doe.com", "jenita123");

- Tabel Categories
INSERT INTO categories(name) VALUES ("gadget"), ("cloth"), ("men"), ("women"), ("branded");

- Tabel items
INSERT INTO items (name, description, price, stock, category_id) VALUES ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1),("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2),("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);


4. Mengambil Data dari Database

a. Mengambil data users
SELECT id, name, email from users;

b. Mengambil data items
- harga > 1000000
SELECT * FROM items WHERE price > 1000000;
- LIKE "sang"
SELECT * FROM items WHERE name LIKE '%sang%';

c. Menampilkan data items join dengan kategori
SELECT items.* , categories.name AS kategori FROM items INNER JOIN categories ON items.category_id = categories.id;


5. Mengubah Data dari Database

UPDATE items SET price = 2500000 WHERE id = 1;
