@extends('layout.master')
@section('title')
    Halaman Edit Cast
@endsection

@section('content')
    <form method="POST" action="/cast/{{$cast->id}}">
        @csrf
        @method('put')
        <div class="form-group">
        <label>Nama</label>
        <input type="text" value="{{$cast->nama}}" name="nama" class="form-control" placeholder="Masukan Nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
        <label>Umur</label>
        <input type="text" value="{{$cast->umur}}" name="umur" class="form-control" placeholder="Masukan Umur">
        </div>
        @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
        <label>Biodata</label>
        <textarea name="bio" cols="30" rows="10" class="form-control">{{$cast->bio}}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
