@extends('layout.master')
@section('title')
    Sign Up
@endsection

@section('content')
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        
        <label for="">First name:</label><br><br>
        <input type="text" name="firstname"><br><br>
        <label for="">Last name:</label><br><br>
        <input type="text" name="lastname"><br><br>
        <label for="">Gender:</label><br><br>
        <input type="radio" name="gender"> Male <br>
        <input type="radio" name="gender"> Female <br>
        <input type="radio" name="gender"> Other <br><br>
        <label for="">Nationality</label><br><br>
        <select name="nationality" id="">
            <option value="">Indonesian</option>
            <option value="">American</option>
            <option value="">Australian</option>
            <option value="">British</option>
            <option value="">Malaysian</option>
            <option value="">Belgian</option>
            <option value="">Other</option>
        </select><br><br>
        <label for="">Languange Spoken:</label><br><br>
        <input type="checkbox" name="language"> Bahasa Indonesia <br>
        <input type="checkbox" name="language"> English <br>
        <input type="checkbox" name="language"> Other <br><br>
        <label for="">Bio:</label><br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection
        
