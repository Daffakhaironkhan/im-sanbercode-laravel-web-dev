<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/welcome', [AuthController::class, 'welcome']);

Route::get('/table',function(){
    return view('page.table');
});

Route::get('/data-tables',function(){
    return view('page.datatable');
});

//CRUD

//Create
Route::get('/cast/create', [CastController::class, 'create']);

//Route save database cast
Route::post('/cast',[CastController::class, 'store']);

//Read
Route::get('/cast',[CastController::class, 'index']);

//Route Detail data
Route::get('/cast/{cast_id}',[CastController::class, 'show']);

//Update
Route::get('/cast/{cast_id}/edit',[CastController::class, 'edit']);

//Update by id
Route::put('/cast/{cast_id}',[CastController::class, 'update']);

//Delete
Route::delete('/cast/{cast_id}',[CastController::class, 'destroy']);
