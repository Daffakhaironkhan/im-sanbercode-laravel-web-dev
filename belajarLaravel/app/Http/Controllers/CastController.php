<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        //Validasi data
        $request->validate([
            'nama' => 'required|min:2',
            'umur' => 'required|min:1',
            'bio' => 'required|min:2',
        ]);

        //Input data
        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        //Lempar
        return redirect('/cast');
    }

    public function index(){
        $cast = DB::table('cast')->get();
        
        return view('cast.read', ['cast' => $cast]);
    }

    public function show($id){
        $cast = DB::table('cast')->find($id);

        return view('cast.detail', ['cast'=>$cast]);
    }

    public function edit($id){
        $cast = DB::table('cast')->find($id);

        return view('cast.edit', ['cast'=>$cast]);
    }

    public function update($id, Request $request){
        //Validasi data
        $request->validate([
            'nama' => 'required|min:2',
            'umur' => 'required|min:1',
            'bio' => 'required|min:2',
        ]);

        //Update
        DB::table('cast')
              ->where('id', $id)
              ->update([
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']
            ]);

        //Lempar
        return redirect('/cast');
    }

    public function destroy($id){
        //Delete
        DB::table('cast')->where('id', $id)->delete();

        //Lempar
        return redirect('/cast');
    }
}
